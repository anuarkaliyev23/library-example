package org.bitbucket.anuarkaliyev23.command;


public interface Command {
    void execute();
}
