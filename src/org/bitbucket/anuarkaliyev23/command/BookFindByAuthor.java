package org.bitbucket.anuarkaliyev23.command;

import org.bitbucket.anuarkaliyev23.model.Author;
import org.bitbucket.anuarkaliyev23.storage.BookStorage;

public class BookFindByAuthor implements Command {
    private Author author;
    private BookStorage storage;

    public BookFindByAuthor(Author author, BookStorage storage) {
        this.author = author;
        this.storage = storage;
    }

    public Author getAuthor() {
        return author;
    }

    public BookStorage getStorage() {
        return storage;
    }

    @Override
    public void execute() {
        storage.getByAuthor(author);
    }
}
