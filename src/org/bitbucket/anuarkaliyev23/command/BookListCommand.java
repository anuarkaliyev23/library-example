package org.bitbucket.anuarkaliyev23.command;

import org.bitbucket.anuarkaliyev23.model.Book;
import org.bitbucket.anuarkaliyev23.storage.BookStorage;

public class BookListCommand implements Command{
    private BookStorage storage;

    public BookListCommand(BookStorage storage) {
        this.storage = storage;
    }

    public BookStorage getStorage() {
        return storage;
    }


    @Override
    public void execute() {
        for (Book b : storage.allBooks()) {
            System.out.println(b);
        }
    }
}
