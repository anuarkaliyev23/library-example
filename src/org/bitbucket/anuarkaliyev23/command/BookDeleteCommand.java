package org.bitbucket.anuarkaliyev23.command;

import org.bitbucket.anuarkaliyev23.storage.BookStorage;

public class BookDeleteCommand implements Command {
    private String title;
    private BookStorage storage;

    public BookDeleteCommand(String title, BookStorage storage) {
        this.title = title;
        this.storage = storage;
    }

    public String getTitle() {
        return title;
    }

    public BookStorage getStorage() {
        return storage;
    }

    @Override
    public void execute() {
        storage.deleteByTitle(title);
    }
}
