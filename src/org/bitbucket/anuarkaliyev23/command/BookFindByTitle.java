package org.bitbucket.anuarkaliyev23.command;

import org.bitbucket.anuarkaliyev23.storage.BookStorage;

public class BookFindByTitle implements Command {
    private String title;
    private BookStorage storage;

    public BookFindByTitle(String title, BookStorage storage) {
        this.title = title;
        this.storage = storage;
    }

    public String getTitle() {
        return title;
    }

    public BookStorage getStorage() {
        return storage;
    }

    @Override
    public void execute() {
        storage.getByTitle(title);
    }
}
