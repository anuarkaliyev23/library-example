package org.bitbucket.anuarkaliyev23.command;

import org.bitbucket.anuarkaliyev23.model.Book;
import org.bitbucket.anuarkaliyev23.storage.BookStorage;

public class BookAddCommand implements Command{
    private Book book;
    private BookStorage storage;

    public BookAddCommand(Book book, BookStorage storage) {
        this.book = book;
        this.storage = storage;
    }

    public Book getBook() {
        return book;
    }

    public BookStorage getStorage() {
        return storage;
    }

    @Override
    public void execute() {
        storage.saveBook(book);
    }
}
