package org.bitbucket.anuarkaliyev23.exception;

public class BookNotFoundException extends RuntimeException {
    private String title;

    public BookNotFoundException(String title) {
        super("Book with title = " + title + ".Not found");
    }
}
