package org.bitbucket.anuarkaliyev23;


import org.bitbucket.anuarkaliyev23.command.Command;
import org.bitbucket.anuarkaliyev23.storage.BookStorage;
import org.bitbucket.anuarkaliyev23.storage.ListBookStorage;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        BookStorage storage = new ListBookStorage();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String line = scanner.nextLine();
            Command command = Parser.parse(line, storage);
            command.execute();
        }
    }
}
