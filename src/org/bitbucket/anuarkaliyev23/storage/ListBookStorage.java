package org.bitbucket.anuarkaliyev23.storage;

import org.bitbucket.anuarkaliyev23.exception.BookNotFoundException;
import org.bitbucket.anuarkaliyev23.model.Author;
import org.bitbucket.anuarkaliyev23.model.Book;

import java.util.ArrayList;
import java.util.List;

public class ListBookStorage implements BookStorage {
    private List<Book> library;

    public ListBookStorage() {
        library = new ArrayList<>();
    }

    public ListBookStorage(List<Book> library) {
        this.library = library;
    }

    public List<Book> getLibrary() {
        return library;
    }

    public void setLibrary(List<Book> library) {
        this.library = library;
    }

    @Override
    public List<Book> allBooks() {
        return library;
    }

    @Override
    public List<Book> getByAuthor(Author author) {
        List<Book> result = new ArrayList<>();
        for (int i = 0; i < library.size(); i++) {
            Book book = library.get(i);
            if (book.getAuthor().equals(author)) {
                result.add(book);
            }
        }
        return result;
    }

    @Override
    public Book getByTitle(String title) {
        for (int i = 0; i < library.size(); i++) {
            Book book = library.get(i);
            if (book.getTitle().equals(title)) {
                return book;
            }
        }
        throw new BookNotFoundException(title);
    }

    @Override
    public void saveBook(Book book) {
        library.add(book);
    }

    @Override
    public void deleteByTitle(String title) {
        Book book = this.getByTitle(title);
        library.remove(book);
    }
}
