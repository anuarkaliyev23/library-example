package org.bitbucket.anuarkaliyev23.storage;

import org.bitbucket.anuarkaliyev23.model.Author;
import org.bitbucket.anuarkaliyev23.model.Book;

import java.util.List;

public interface BookStorage {
    List<Book> allBooks();
    List<Book> getByAuthor(Author author);
    Book getByTitle(String title);
    void saveBook(Book book);
    void deleteByTitle(String title);
}