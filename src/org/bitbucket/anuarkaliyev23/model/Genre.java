package org.bitbucket.anuarkaliyev23.model;

import java.util.Enumeration;

//Fantasy
//Novel
//History
//Horror
//Scientific
public enum Genre {
    FANTASY,
    NOVEL,
    HISTORY,
    HORROR,
    SCIENTIFIC;
}
