package org.bitbucket.anuarkaliyev23;

import org.bitbucket.anuarkaliyev23.command.BookAddCommand;
import org.bitbucket.anuarkaliyev23.command.BookListCommand;
import org.bitbucket.anuarkaliyev23.command.Command;
import org.bitbucket.anuarkaliyev23.model.Author;
import org.bitbucket.anuarkaliyev23.model.Book;
import org.bitbucket.anuarkaliyev23.model.Genre;
import org.bitbucket.anuarkaliyev23.storage.BookStorage;

import java.time.LocalDate;
import java.util.Scanner;

public class Parser {
    static Author parseAuthor(String firstName, String lastName, String middleName, String birthday, String deathday, String sex) {
        Author author = new Author();
        author.setFirstName(firstName);
        author.setLastName(lastName);
        author.setMiddleName(middleName);
        author.setMale(sex.equals("male"));
        author.setBirthDate(LocalDate.parse(birthday));
        if (deathday.isEmpty()) {
            author.setDeathDate(null);
        } else {
            author.setDeathDate(LocalDate.parse(deathday));
        }
        return author;
    }

    static BookAddCommand parseBookAddCommand(String line, BookStorage storage) {
        String[] tokens = line.split("[,]");
        String bookTitle = tokens[0].trim();
        String authorFirstName = tokens[1].trim();
        String authorLastName = tokens[2].trim();
        String authorMiddleName = tokens[3].trim();
        String birthdayString = tokens[4].trim();
        String deathDayString = tokens[5].trim();
        String sexString = tokens[6].trim();
        String publicationDateString = tokens[7].trim();
        String genreString = tokens[8].trim();

        Author author = parseAuthor(authorFirstName, authorLastName, authorMiddleName, birthdayString, deathDayString, sexString);
        LocalDate publicationDate = LocalDate.parse(publicationDateString);
        Genre genre = Genre.valueOf(genreString.toUpperCase());

        Book book = new Book();
        book.setTitle(bookTitle);
        book.setAuthor(author);
        book.setPublicationDate(publicationDate);
        book.setGenre(genre);

        return new BookAddCommand(book, storage);
    }

    static Command parse(String line, BookStorage storage) {
        Scanner scanner = new Scanner(line);
        String firstToken = scanner.next();
        if (!firstToken.equals("book"))
            throw new RuntimeException("Not supported command");

        String secondToken = scanner.next();
        switch (secondToken) {
            case "add": return parseBookAddCommand(scanner.nextLine(), storage);
            case "list": return new BookListCommand(storage);
            default: throw new RuntimeException("Not supported command!");
        }

    }
}
